"""casaorganica URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include, patterns
from django.conf.urls.i18n import urlpatterns
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from web import views

urlpatterns = [
    url(r'^web/', include('web.urls')),
    url(r'^loginuser$', views.login_user, name='login_user'),
    url(r'^logout$', views.logout_user, name='logout_user'),
    url(r'^aceptarOferta$', views.aceptarOferta, name='aceptarOferta'),
    url(r'^rechazarOferta$', views.rechazarOferta, name='rechazarOferta'),
    url(r'^home', views.home, name='login_user'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', views.index, name='index'),

    url(r'^login$', views.login_user_rest, name='login_user_rest'),
    url(r'^products$', views.oferta_semana_list_rest, name='oferta_semana_list'),
    url(r'^cart/add', views.carrito_add_rest, name='carrito_add_rest'),
    url(r'^order', views.carrito_items_rest, name='carrito_items_rest'),
    url(r'^cart/checkout', views.carrito_checkout_rest, name='carrito_checkout_rest'),
    url(r'^order/schedule', views.order_schedule_rest, name='order_schedule_rest'),

]

urlpatterns += patterns('',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT,
    }),
 )

urlpatterns += patterns('',
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.STATIC_ROOT,
    }),
 )

