from django.contrib import admin
from .models import ProductoForm, Administrador, Producto, Unidad, Productor, OfertaProducto, TipoProducto, Consumidor, Ciudad, \
    HorarioDistribucion

# Register your models here.

admin.site.register(Producto);
admin.site.register(Unidad);
admin.site.register(Productor);
admin.site.register(OfertaProducto);
admin.site.register(TipoProducto);
admin.site.register(Consumidor);
admin.site.register(Ciudad);
admin.site.register(HorarioDistribucion);
admin.site.register(Administrador);