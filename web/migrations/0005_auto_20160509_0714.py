# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-09 12:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('web', '0004_auto_20160419_2338'),
    ]

    operations = [
        migrations.CreateModel(
            name='HorarioDistribucion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('dia', models.DateField()),
                ('horaInicio', models.TimeField()),
                ('horaFin', models.TimeField()),
            ],
        ),
        migrations.AddField(
            model_name='ordencompra',
            name='horarioDistribucion',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='web.HorarioDistribucion'),
        ),
    ]
