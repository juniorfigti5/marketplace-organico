from celery.schedules import crontab
from celery.task import periodic_task
from celery.utils.log import get_task_logger
from django.core.mail import EmailMessage
from datetime import datetime, date, timedelta

from models import OfertaProducto

logger = get_task_logger(__name__)


@periodic_task(
    run_every=(crontab(minute=0, hour=0, day_of_week='sunday')),
    name="notificar_cierre_oferta_task",
    ignore_result=True
)
def notificar_cierre_oferta_task():
    fecha_inicio = datetime.today().date() - timedelta(days=datetime.today().weekday())
    fecha_fin = fecha_inicio - timedelta(days=7)
    lista_ofertas = OfertaProducto.objects.filter(fechaOferta__range=(fecha_fin, fecha_inicio))

    for oferta in lista_ofertas:
        email = EmailMessage('Oferta Cerrada: ' + [oferta.producto.nombres], 'Hola ' + [oferta.productor.nombres]
                             + '. La oferta de ' + [oferta.producto.nombres] + ' que realizaste el ' + [oferta.fechaOferta]
                             + ' se ha cerrado. Por favor ingresa a la plataforma para confirmarla!', to=[oferta.productor.correo])
        email.send()

