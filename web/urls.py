from django.conf import settings
from django.conf.urls import url, include, patterns
from django.conf.urls.i18n import urlpatterns
from django.contrib import admin
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^index$', views.login_user, name='login_user'),
    url(r'^home', views.index, name='index'),
    url(r'^logout$', views.logout_user, name='logout_user'),
    url(r'^Productor/home$', views.home, name='login_user'),
    url(r'^Administrador/homeAdmin$', views.homeAdmin, name='login_user'),
    url(r'^registroConsumidor', views.registroConsumidor, name='registroConsumidor'),
    url(r'^registroProductor', views.registroProductor, name='registroProductor'),
    url(r'^$', views.index, name='index'),
    url(r'^Producto/listProducts$', views.consultaProductos, name='consultaProductos'),
    url(r'^Productor/ofertaproducto/(?P<id>[0-9]+)/$', views.ofertarProducto, name='ofertarProducto'),
    url(r'^Productor/ofertaproductos/(?P<page>[0-9]+)/$', views.ofertaProductos, name='ofertaProducto'),
    url(r'^quienes$', views.quienes, name='quienes'),
    url(r'^carrito$', views.carrito, name='carrito'),
    url(r'^web/carrito$', views.carrito, name='carrito'),
    url(r'^checkout$', views.checkout, name='checkout'),
    url(r'^web/checkout$', views.checkout, name='checkout'),
    url(r'^nuestros$', views.nuestros, name='nuestros'),
    url(r'^Productor/productos$', views.productosOferta, name='productosProductor'),
    url(r'^Productor/oferta/(?P<id>[0-9]+)/$', views.ofertaProducto, name='ofertaProductor'),
    url(r'^Productor/ofertacancelar/$', views.ofertaProductoCancelar, name='ofertaProductoCancelar'),
    url(r'^Consumidor/edit$', views.consumidorEdit, name='consumidorEdit'),
    url(r'^Productor/pedidos/(?P<page>[0-9]+)/$', views.consultaPedidos, name='ofertaProducto'),

    url(r'^rest/oferta/$', views.oferta_list, name='oferta_list'),
    url(r'^rest/ofertas/(?P<pk>[0-9]+)$', views.oferta_detail, name='oferta_detail'),
    url(r'^rest/producto/$', views.producto_list, name='producto_list'),
    url(r'^rest/productos/(?P<pk>[0-9]+)$', views.producto_detail, name='producto_detail'),
    url(r'^rest/productor/$', views.productor_list, name='productor_list'),
    url(r'^rest/productores/(?P<pk>[0-9]+)$', views.productor_detail, name='productor_detail'),
    url(r'^rest/horarios/(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d+)/$', views.horarios_list, name='oferta_list')
]