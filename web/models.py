from __future__ import unicode_literals

from datetime import datetime

from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User

# Create your models here.

class Administrador(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    telefono = models.CharField(max_length=100)
    correo = models.EmailField()
    usuario = models.ForeignKey(User, null=False)

    def __unicode__(self):
        return self.nombres + ' ' + self.apellidos

class Unidad(models.Model):
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre

class TipoProducto(models.Model):
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre

class Producto(models.Model):
    nombres = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=500)
    foto = models.ImageField(upload_to="casaorganica/images")
    precio = models.IntegerField(default=0)
    unidad = models.ForeignKey(Unidad, on_delete=models.CASCADE)
    tipoProducto = models.ForeignKey(TipoProducto, on_delete=models.CASCADE)

    def __unicode__(self):
        return self.nombres


class Ciudad(models.Model):
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre
class Productor(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    telefono = models.CharField(max_length=100)
    correo = models.EmailField()
    direccion = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, null=False)

    def __unicode__(self):
        return self.nombres + ' ' + self.apellidos





class Consumidor(models.Model):
    nombres = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    correo = models.EmailField()
    direccion = models.CharField(max_length=100)
    ciudad = models.ForeignKey(Ciudad, on_delete=models.CASCADE)
    usuario = models.ForeignKey(User, null=False)

    def __unicode__(self):
        return self.nombres + ' ' + self.apellidos

class OfertaProducto(models.Model):
    cantidad = models.IntegerField(default=0)
    precio = models.DecimalField(decimal_places=2, max_digits=12)
    producto = models.ForeignKey(Producto, null=False)
    productor = models.ForeignKey(Productor, on_delete=models.CASCADE)
    fechaOferta = models.DateTimeField(default=datetime.now)


    @property
    def costo_total(self):
        return self.cantidad * self.precio

class MedioPago(models.Model):
    nombre = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nombre

class CarritoItem(models.Model):
    cantidad = models.IntegerField(default=0)
    producto = models.ForeignKey(OfertaProducto, null=False)
    precio = models.FloatField(default=0)

class Carrito(models.Model):
    items = models.ManyToManyField(CarritoItem)
    comprador = models.ForeignKey(Consumidor, null=False)
    total = models.IntegerField(default=0)
    activo = models.BooleanField(default=False, null=False)

class HorarioDistribucion(models.Model):
    dia = models.DateField(null=False)
    horaInicio = models.TimeField(null=False)
    horaFin = models.TimeField(null=False)

class OrdenCompra(models.Model):
    carrito = models.ForeignKey(Carrito, null=False)
    fechaCompra = models.DateTimeField(default=datetime.now)
    horarioDistribucion = models.ForeignKey(HorarioDistribucion, null=True)

class ProductoForm(ModelForm):

    class Meta:
        model=Producto
        fields=['nombres', 'descripcion', 'foto', 'precio', 'unidad','tipoProducto']

class BusquedaForm(ModelForm):

    class Meta:
        model=Producto
        fields=['precio','tipoProducto']
