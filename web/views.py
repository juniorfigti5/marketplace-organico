from django.contrib.auth import authenticate, login, logout
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib import messages
from datetime import datetime, date, timedelta

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

import json

from models import Producto, ProductoForm, Administrador, BusquedaForm, Productor, OfertaProducto, Consumidor, Ciudad, Carrito, CarritoItem, MedioPago, OrdenCompra, \
    HorarioDistribucion

# Create your views here.
from web.serializers import OfertaSerializer, ProductoSerializer, ProductorSerializer, HorarioSerializer, ProductoOfertaSerializer, CarritoItemSerializer
from django.contrib.auth.models import User
from django.core.validators import validate_email
from django import forms

def index(request):


    if request.user.is_authenticated():
        current_user = request.user
        try:
            productor = Productor.objects.get(usuario=current_user)
            if productor is not None:
                return redirect('/web/Productor/home')
        except Productor.DoesNotExist:
            pass


    lista_productos = OfertaProducto.objects.filter()
    form = BusquedaForm()
    paginador = Paginator(lista_productos, 3)

    if request.method == 'POST':
        if 'agregarCarritoButton' in request.POST:

            if request.user.is_authenticated():

                consumidor = Consumidor.objects.get(usuario=request.user)

                try:
                    carritoActual = Carrito.objects.get(comprador=consumidor, activo=True)
                except Carrito.DoesNotExist:
                    carritoActual = Carrito()
                    carritoActual.comprador = consumidor
                    carritoActual.activo = True
                    carritoActual.save()

                carritoItem = CarritoItem()
                carritoItem.cantidad = int(request.POST['numeroItemsAgregar'])
                prod = OfertaProducto.objects.get(id=int(request.POST['idItemAgregar']))
                carritoItem.producto = prod
                carritoItem.precio = int(request.POST['numeroItemsAgregar'])*prod.precio
                carritoItem.save()

                carritoActual.items.add(carritoItem)
                carritoActual.total = carritoActual.total + carritoItem.precio
                carritoActual.save()

    page = request.GET.get('page')

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_productos': lista_paginada, 'user_auth': request.user.is_authenticated(), 'form': form}

    return render(request, 'web/index.html', context)


def quienes(request):
    return render(request, 'web/quienes.html')

def checkout(request):

    if request.method == 'POST':
        if 'checkoutButton' in request.POST:
            consumidor = Consumidor.objects.get(usuario=request.user)
            horario = request.POST.get('horarioId')

            orden = OrdenCompra()
            orden.carrito = Carrito.objects.get(comprador=consumidor, activo=True)
            #orden.fechaCompra = datetime.now
            orden.horarioDistribucion = HorarioDistribucion.objects.get(id=int(horario))
            orden.save()

            carro = Carrito.objects.get(comprador=consumidor, activo=True)

            for item in carro.items.all():
                print(item.id)
                carItem = CarritoItem.objects.get(id=item.id)
                prod = carItem.producto
                prod.cantidad = carItem.producto.cantidad - carItem.cantidad
                prod.save()
                carItem.save()

            carro.activo = False
            carro.save()

            messages.add_message(request, messages.INFO, 'Tu orden se ha realizado correctamente. Pronto entregaremos tus productos!', 'success')
            return redirect('index')

    return render(request, 'web/checkout.html')

def carrito(request):

    consumidor = Consumidor.objects.get(usuario=request.user)

    try:
        carritoActual = Carrito.objects.get(comprador=consumidor, activo=True)
        carritoItems = carritoActual.items
    except Carrito.DoesNotExist:
        carritoActual = Carrito()
        carritoItems = []

    context = {'carrito': carritoActual, 'carrito_items': carritoItems, 'user_auth': request.user.is_authenticated()}

    return render(request, 'web/carrito.html',context)

def nuestros(request):
    return render(request, 'web/nuestros.html')


def logout_user(request):
    logout(request)
    return redirect('index')

def homeAdmin(request):
    return render(request, 'web/Administrador/homeAdmin.html')

def aceptarOferta(request):

    return redirect('index')

def rechazarOferta(request):
    fecha_inicio = datetime.today().date() - timedelta(days=datetime.today().weekday())
    fecha_fin = fecha_inicio + timedelta(days=7)
    current_user = request.user
    productorAct = Productor.objects.get(usuario=current_user)
    lista_ofertas = OfertaProducto.objects.filter(productor = productorAct, fechaOferta__range=(fecha_inicio, fecha_fin))

    for OfertaProduct in lista_ofertas:
        OfertaProduct.delete()

    return redirect('index')

def login_user(request):
    if request.user.is_authenticated():
        return redirect('/web/Productor/home')

    mensaje = ''
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None and user.is_active:
            login(request, user)
            current_user = request.user

            try:
                consumidor = Consumidor.objects.get(usuario=current_user)
                if consumidor is not None:
                    return redirect('index')
            except Consumidor.DoesNotExist:
                pass

            try:
                productor = Productor.objects.get(usuario=current_user)
                if productor is not None:
                    return redirect('/web/Productor/home')
            except Productor.DoesNotExist:
                pass

            try:
                administrador = Administrador.objects.get(usuario=current_user)
                if administrador is not None:
                    return redirect('/web/Administrador/homeAdmin')
            except:
                pass


        messages.add_message(request, messages.ERROR, 'Nombre de usuario o clave no valido', 'danger')

    ##return render(request, 'web/index.html', {'mensaje': mensaje})
    return redirect('index')


def home(request):
    if not request.user.is_authenticated():
        return redirect('index')
    else:

        page = request.GET.get('page')
        fecha_inicio = datetime.today().date() - timedelta(days=datetime.today().weekday())
        fecha_fin = fecha_inicio + timedelta(days=7)

        current_user = request.user

        productor = Productor.objects.get(usuario=current_user)
        lista_ofertas = OfertaProducto.objects.filter(productor=productor, fechaOferta__range=(fecha_inicio, fecha_fin))
        paginador = Paginator(lista_ofertas, 5)

        try:
            lista_paginada = paginador.page(page)
        except PageNotAnInteger:
            lista_paginada = paginador.page(1)
        except EmptyPage:
            lista_paginada = paginador.page(paginador.num_pages)

        context = {'lista_ofertas': lista_paginada, 'user_auth': request.user.is_authenticated(),
                   'finicio': fecha_inicio + timedelta(days=7), 'ffin': fecha_fin + timedelta(days=6)}

    return render(request, 'web/Productor/home.html', context)


@csrf_exempt
def new_Product_json(request):
    if request.method == 'POST':
        jsonProducto = json.loads(request.body)

        producto = Producto()
        producto.nombres = jsonProducto['nombres']
        producto.descripcion = jsonProducto['descripcion']
        producto.foto = jsonProducto['foto']
        producto.precio = jsonProducto['precio']
        producto.unidad = jsonProducto['unidad']
        producto.save()

        return redirect(reverse('index'))
    return redirect('index')


def consultaProductos(request):
    today = datetime.today()
    weekday = today.weekday()
    start_delta = timedelta(days=(weekday + 8))
    start_of_week = today - start_delta

    # fecha_inicio = datetime.today()-timedelta(days=7)
    # fecha_fin = datetime.today()

    fecha_inicio = start_of_week
    fecha_fin = start_of_week + timedelta(days=7)

    if request.method == 'POST':
        if 'filtrar' in request.POST:
            lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin),
                                                            producto__precio=request.POST['precio'])
            semana_siguiente = False
        elif 'clear' in request.POST:
            lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin))
            semana_siguiente = False
        elif 'semana2' in request.POST:
            lista_productos = OfertaProducto.objects.filter(
                fechaOferta__range=((fecha_inicio + timedelta(days=7)), (fecha_fin + timedelta(days=7))))
            semana_siguiente = True
        elif 'semana1' in request.POST:
            lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin))
            semana_siguiente = False
    else:
        lista_productos = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin))
        semana_siguiente = False

    form = BusquedaForm()
    paginador = Paginator(lista_productos, 4)

    page = request.GET.get('page')

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'semana_siguiente': semana_siguiente, 'lista_productos': lista_paginada,
               'user_auth': request.user.is_authenticated(), 'form': form}

    return render(request, 'web/Producto/listProducts.html', context)


def ofertarProducto(request, id):
    if request.method == 'POST':

        producto = Producto.objects.get(id=id)
        current_user = request.user
        productor = Productor.objects.get(usuario=current_user)

        oferta = OfertaProducto()
        oferta.producto = producto
        oferta.productor = productor
        oferta.cantidad = request.POST.get('Cantidad')
        oferta.precio = request.POST.get('Precio')
        oferta.save()

        messages.add_message(request, messages.INFO, 'La Oferta Ha Sido Creada', 'success')
        return redirect('/web/Productor/home')

    else:
        producto = Producto.objects.get(id=id)

    context = {'producto': producto}
    return render(request, 'web/Productor/newOferta.html', context)


def ofertaProductos(request, page):
    fecha_inicio = datetime.today().date() - timedelta(days=datetime.today().weekday())
    fecha_fin = fecha_inicio + timedelta(days=7)

    current_user = request.user
    productor = Productor.objects.get(usuario=current_user)
    lista_ofertas = OfertaProducto.objects.filter(productor=productor, fechaOferta__range=(fecha_inicio, fecha_fin))
    paginador = Paginator(lista_ofertas, 5)

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_ofertas': lista_paginada, 'user_auth': request.user.is_authenticated(),
               'finicio': fecha_inicio + timedelta(days=7), 'ffin': fecha_fin + timedelta(days=6)}
    return render(request, 'web/Productor/listaOferta.html', context)


def productosOferta(request):
    if request.method == 'POST':
        lista_productos = Producto.objects.filter(precio=request.POST['precio'])
    else:
        lista_productos = Producto.objects.filter()

    form = BusquedaForm()
    paginador = Paginator(lista_productos, 3)

    page = request.GET.get('page')

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_productos': lista_paginada, 'user_auth': request.user.is_authenticated(), 'form': form}

    return render(request, 'web/Productor/listaProductos.html', context)


def ofertaProducto(request, id):
    if request.method == 'POST':
        oferta = OfertaProducto.objects.get(id=id)
        oferta.cantidad = request.POST.get('Cantidad')
        oferta.precio = request.POST.get('Precio')
        oferta.save()

        messages.add_message(request, messages.INFO, 'La Oferta Ha Sido Modificada', 'success')
        return redirect('/web/Productor/home')

    else:
        oferta = OfertaProducto.objects.get(pk=id)
        producto = oferta.producto

    context = {'producto': producto, 'oferta': oferta}
    return render(request, 'web/Productor/newOferta.html', context)


def ofertaProductoCancelar(request):
    if request.method == 'POST':
        oferta = OfertaProducto.objects.get(id=request.POST.get('oid'))
        oferta.delete()
        messages.add_message(request, messages.INFO, 'La Oferta Ha Sido Eliminada', 'success')

    return redirect('/web/Productor/home')


def registroConsumidor(request):
    mensaje = ''
    if request.method == 'GET':
        ciudades = Ciudad.objects.all()
        return render(request, 'web/registroConsumidor.html', {'mensaje': mensaje, 'ciudades': ciudades})

    if request.method == 'POST':
        try:
           User.objects.get(username=request.POST.get('usuario'))
        except ObjectDoesNotExist:

            try:
                validate_email(request.POST.get("correo", ""))
            except forms.ValidationError:
                messages.add_message(request, messages.ERROR, 'Ingrese un Email Valido', 'danger')
                return redirect('/web/registroConsumidor')

            pass1 = request.POST.get('password')
            pass2 = request.POST.get('password_confirm')
            if pass1 != pass2:
                messages.add_message(request,messages.ERROR,'Las contrasenas deben Coincidir','danger')
                return redirect('/web/registroConsumidor')

            user = User.objects.create_user(request.POST.get('usuario'),request.POST.get('correo'),request.POST.get('password'))
            user.first_name = request.POST.get('nombres')
            user.last_name = request.POST.get('apellidos')
            user.save()

            consumidor = Consumidor()
            consumidor.usuario = user
            consumidor.nombres = request.POST.get('nombres')
            consumidor.apellidos = request.POST.get('apellidos')
            consumidor.correo = request.POST.get('correo')
            consumidor.ciudad = Ciudad.objects.get(id=request.POST.get('ciudad'))
            consumidor.direccion = request.POST.get('direccion')
            consumidor.save()

            messages.add_message(request, messages.INFO, 'Usuario Registrado', 'success')
            return redirect('/web/registroConsumidor')

        messages.add_message(request, messages.ERROR, 'El nombre de usuario ya existe', 'danger')
        return redirect('/web/registroConsumidor')

    return render(request, 'web/registroConsumidor.html', {'mensaje': mensaje})

def registroProductor(request):
    mensaje = ''
    if request.method == 'GET':
        ciudades = Ciudad.objects.all()
        return render(request, 'web/registroProductor.html', {'mensaje': mensaje, 'ciudades': ciudades})

    if request.method == 'POST':
        try:
           User.objects.get(username=request.POST.get('usuario'))
        except ObjectDoesNotExist:

            try:
                validate_email(request.POST.get("correo", ""))
            except forms.ValidationError:
                messages.add_message(request, messages.ERROR, 'Ingrese un Email Valido', 'danger')
                return redirect('/web/registroProductor')

            pass1 = request.POST.get('password')
            pass2 = request.POST.get('password_confirm')
            if pass1 != pass2:
                messages.add_message(request,messages.ERROR,'Las contrasenas deben Coincidir','danger')
                return redirect('/web/registroProductor')

            user = User.objects.create_user(request.POST.get('usuario'),request.POST.get('correo'),request.POST.get('password'))
            user.first_name = request.POST.get('nombres')
            user.last_name = request.POST.get('apellidos')
            user.save()

            productor = Productor()
            productor.usuario = user
            productor.nombres = request.POST.get('nombres')
            productor.apellidos = request.POST.get('apellidos')
            productor.correo = request.POST.get('correo')
            productor.telefono = request.POST.get('telefono')
            productor.ciudad = Ciudad.objects.get(id=request.POST.get('ciudad'))
            productor.direccion = request.POST.get('direccion')
            productor.save()

            messages.add_message(request, messages.INFO, 'Productor Registrado', 'success')
            return redirect('/web/registroProductor')

        messages.add_message(request, messages.ERROR, 'El nombre de usuario ya existe', 'danger')
        return redirect('/web/registroProductor')

    return render(request, 'web/registrProductor.html', {'mensaje': mensaje})

def consumidorEdit(request):
    if not request.user.is_authenticated():
        return redirect('index')

    mensaje = ''
    try:
        consumidor = Consumidor.objects.get(usuario=request.user)
    except ObjectDoesNotExist:
        messages.add_message(request, messages.ERROR, 'No es un usuario consumidor', 'danger')
        return redirect('index')

    if request.method == 'GET':
        ciudades = Ciudad.objects.all()
        return render(request, 'web/Consumidor/edit.html',
                      {'mensaje': mensaje, 'consumidor': consumidor, 'ciudades': ciudades})

    if request.method == 'POST':
        consumidor.nombres = request.POST.get('nombres')
        consumidor.apellidos = request.POST.get('apellidos')
        consumidor.correo = request.POST.get('correo')
        consumidor.ciudad = Ciudad.objects.get(id=request.POST.get('ciudad'))
        consumidor.direccion = request.POST.get('direccion')
        consumidor.save()

        messages.add_message(request, messages.INFO, 'Su informacion Ha Sido Modificada', 'success')
        return redirect('/web/Consumidor/edit')

    return render(request, 'web/Consumidor/edit.html', {'mensaje': mensaje})


@api_view(['GET', 'PUT', 'DELETE'])
def oferta_detail(request, pk):
    """
    REST Consulta, actualiza o borra a una oferta producto.
    """
    try:
        oferta = OfertaProducto.objects.get(pk=pk)
    except OfertaProducto.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = OfertaSerializer(oferta)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = OfertaSerializer(oferta, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        oferta.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def oferta_list(request):
    """
    Lista Ofertas, o la crea.
    """
    if request.method == 'GET':
        oferta = OfertaProducto.objects.all()
        serializer = OfertaSerializer(oferta, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = OfertaSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def producto_detail(request, pk):
    """
    REST Consulta, actualiza o borra a un producto.
    """
    try:
        producto = Producto.objects.get(pk=pk)
    except Producto.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProductoSerializer(producto)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductoSerializer(producto, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        producto.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def producto_list(request):
    """
    Lista Ofertas, o la crea.
    """
    if request.method == 'GET':
        producto = Producto.objects.all()
        serializer = ProductoSerializer(producto, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ProductoSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
def productor_detail(request, pk):
    """
    REST Consulta, actualiza o borra a un productor.
    """
    try:
        productor = Productor.objects.get(pk=pk)
    except Productor.DoesNotExist:
        return HttpResponse(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ProductorSerializer(productor)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = ProductorSerializer(productor, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        productor.delete()
        return HttpResponse(status=status.HTTP_204_NO_CONTENT)


@api_view(['GET', 'POST'])
def productor_list(request):
    """
    Lista Ofertas, o la crea.
    """
    if request.method == 'GET':
        productor = Productor.objects.all()
        serializer = ProductorSerializer(productor, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = ProductorSerializer(data=request.DATA)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST)


def consultaPedidos(request, page):

    fecha_inicio = datetime.today().date()-timedelta(days=datetime.today().weekday())
    fecha_fin = fecha_inicio + timedelta(days=7)
    es_domingo = datetime.today().weekday()==6

    current_user = request.user
    productor = Productor.objects.get(usuario=current_user)
    lista_ofertas = CarritoItem.objects.filter(producto__productor=productor, carrito__ordencompra__fechaCompra__range=(fecha_inicio, fecha_fin))
    paginador = Paginator(lista_ofertas, 5)

    try:
        lista_paginada = paginador.page(page)
    except PageNotAnInteger:
        lista_paginada = paginador.page(1)
    except EmptyPage:
        lista_paginada = paginador.page(paginador.num_pages)

    context = {'lista_pedidos': lista_paginada, 'user_auth':request.user.is_authenticated(), 'finicio':fecha_inicio,
               'ffin':fecha_fin - timedelta(days=1), 'domingo' : es_domingo }
    return render(request, 'web/Productor/listaPedidos.html', context)

@api_view(['POST'])
def login_user_rest(request):

    status = 'ERROR'
    message = 'Usuario o Clave incorrecta'

    username = request.data.get('username')
    password = request.data.get('password')

    user = authenticate(username=username, password=password)
    if user is not None and user.is_active:
        try:
            consumidor = Consumidor.objects.get(usuario=user)
            if consumidor is not None:
                login(request, user)
                status = 'OK'
                message = 'El usuario ha iniciado sesion'

        except Consumidor.DoesNotExist:
            pass

    content = {'username': username, 'status' : status, 'message' : message }
    return Response(content)

@api_view(['GET'])
def oferta_semana_list_rest(request):

    page = request.GET.get('page')
    fecha_inicio = datetime.today().date() - timedelta(days=datetime.today().weekday()) - timedelta(weeks=1)
    fecha_fin = fecha_inicio + timedelta(days=7)
    lista_ofertas = OfertaProducto.objects.filter(fechaOferta__range=(fecha_inicio, fecha_fin))

    if page is not None:
        paginador = Paginator(lista_ofertas, 10)

        try:
            lista_paginada = paginador.page(page)
        except PageNotAnInteger:
            lista_paginada = paginador.page(1)
        except EmptyPage:
            lista_paginada = paginador.page(paginador.num_pages)

        lista = lista_paginada
    else:
        lista = lista_ofertas

    serializer = ProductoOfertaSerializer(lista, many=True)
    return Response(serializer.data)

@api_view(['PUT'])
def carrito_add_rest(request):

    if not request.user.is_authenticated() or request.data.get('username') is None or request.user.username.lower() != request.data.get('username').lower():
        return Response({'status' : 'ERROR', 'message' : 'Usuario no autenticado' })

    try:
        consumidor = Consumidor.objects.get(usuario=request.user)
    except Consumidor.DoesNotExist:
        return Response({'status' : 'ERROR', 'message' : 'Usuario no autenticado' })

    try:
        carritoActual = Carrito.objects.get(comprador=consumidor, activo=True)
    except Carrito.DoesNotExist:
        carritoActual = Carrito()
        carritoActual.comprador = consumidor
        carritoActual.activo = True
        carritoActual.save()

    try:
        oferta = OfertaProducto.objects.get(id=request.data.get('id_product'))
    except (OfertaProducto.DoesNotExist, ValueError):
        return Response({'status' : 'ERROR', 'message':'Oferta de producto no existe'})

    fecha_inicio = datetime.today().date() - timedelta(days=datetime.today().weekday()) - timedelta(weeks=1)
    fecha_fin = fecha_inicio + timedelta(days=7)
    if oferta.fechaOferta.date() < fecha_inicio or oferta.fechaOferta.date() >= fecha_fin:
        return Response({'status' : 'ERROR', 'message' : 'Oferta no disponible para la semana actual'})

    items = 0
    try:
        items = int(request.data.get('quantity'))
    except ValueError:
        pass

    if items <= 0 or items > oferta.cantidad:
        return Response({'status' : 'ERROR', 'message' : 'Cantidad de productos invalida'})

    carritoItem = CarritoItem()
    carritoItem.cantidad = items
    prod = oferta
    carritoItem.producto = prod
    carritoItem.precio = items * prod.precio
    carritoItem.save()

    carritoActual.items.add(carritoItem)
    carritoActual.total += carritoItem.precio
    carritoActual.save()
    return Response({'status' : 'OK', 'message' : 'Agregado al carrito'})


@api_view(['GET'])
def carrito_items_rest(request):
    if not request.user.is_authenticated() :
        return Response(CarritoItemSerializer(None, many=True).data)

    try:
        consumidor = Consumidor.objects.get(usuario=request.user)
    except Consumidor.DoesNotExist:
        return Response(CarritoItemSerializer(None, many=True).data)

    try:
        carritoActual = Carrito.objects.get(comprador=consumidor, activo=True)
    except Carrito.DoesNotExist:
        return Response(CarritoItemSerializer(None, many=True).data)

    return Response(CarritoItemSerializer(carritoActual.items, many=True).data)

@api_view(['POST'])
def carrito_checkout_rest(request):

    if not request.data.get('confirmation') is True:
        return Response({'status' : 'ERROR', 'message' : 'No se recibio confirmacion'})

    username = request.data.get('username')
    password = request.data.get('password')

    user = authenticate(username=username, password=password)
    if user is None or not user.is_active:
        return Response({'status' : 'ERROR', 'message' : 'Usuario o Clave incorrecta'})

    try:
        consumidor = Consumidor.objects.get(usuario=user)
    except Consumidor.DoesNotExist:
        return Response({'status' : 'ERROR', 'message' : 'Usuario o Clave incorrecta'})

    existeCarrito = False
    try:
        carritoActual = Carrito.objects.get(comprador=consumidor, activo=True)
        existeCarrito = True
    except Carrito.DoesNotExist:
        pass

    if not existeCarrito or carritoActual.items.count() == 0:
        return Response({'status' : 'ERROR', 'message' : 'No hay elementos en el carrito de compras'})

    try:
        with transaction.atomic():
            for item in carritoActual.items.all():
                carItem = CarritoItem.objects.get(id=item.id)
                prod = carItem.producto
                prod.cantidad = carItem.producto.cantidad - carItem.cantidad

                if prod.cantidad < 0:
                    raise IntegrityError

                prod.save()
                carItem.save()

            carritoActual.activo = False
            carritoActual.save()

            orden = OrdenCompra()
            orden.carrito = carritoActual
            orden.save()

    except IntegrityError:
        return Response({'status' : 'ERROR', 'message' : 'Producto Agotado'})

    return Response({'status' : 'OK', 'message' : 'Confirmada la compra'})

@api_view(['GET'])
def order_schedule_rest(request):
    return Response({'status' : 'OK'})


@api_view(['GET'])
def horarios_list(request, year, month, day):
    horario = HorarioDistribucion.objects.filter(dia=date(int(year), int(month), int(day)))
    serializer = HorarioSerializer(horario, many=True)
    return Response(serializer.data)