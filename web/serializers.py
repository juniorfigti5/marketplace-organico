from rest_framework import serializers

from casaorganica.settings import MEDIA_URL
from web.models import Producto, Productor, OfertaProducto, CarritoItem, HorarioDistribucion


class HorarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = HorarioDistribucion
        fields = ('dia', 'horaInicio', 'horaFin', 'id')

class ProductoSerializer(serializers.ModelSerializer):

    unidad = serializers.StringRelatedField(many=False)
    tipoProducto = serializers.StringRelatedField(many=False)

    class Meta:
        model = Producto
        fields = ('nombres', 'descripcion', 'precio', 'foto', 'unidad', 'tipoProducto')

class ProductorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Productor
        fields = ('nombres', 'apellidos', 'telefono', 'correo')

class OfertaSerializer(serializers.ModelSerializer):

    producto = ProductoSerializer(many=False, read_only=True)
    productor = ProductorSerializer(many=False, read_only=True)

    class Meta:
        model = OfertaProducto
        fields = ('id', 'cantidad', 'precio', 'fechaOferta', 'producto', 'productor')

class ProductoOfertaSerializer(serializers.ModelSerializer):

    name = serializers.CharField(source='producto.nombres', read_only=True)
    price = serializers.SerializerMethodField()
    unit = serializers.CharField(source='producto.unidad', read_only=True)
    premium = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    stock = serializers.IntegerField(source='cantidad', read_only=True)

    def get_price(self, obj):
        return obj.precio

    def get_premium(self, obj):
        return True

    def get_image(self, obj):
        return obj.producto.foto.url

    class Meta:
        model = OfertaProducto
        fields = ('id', 'name', 'price', 'unit', 'premium', 'image', 'stock')


class CarritoItemSerializer(serializers.ModelSerializer):

    name = serializers.CharField(source='producto.producto.nombres', read_only=True)
    quantity = serializers.IntegerField(source='cantidad', read_only=True)
    unit = serializers.CharField(source='producto.producto.unidad', read_only=True)
    total = serializers.SerializerMethodField()
    price = serializers.SerializerMethodField()

    def get_total(self, obj):
        return obj.precio

    def get_price(self, obj):
        return obj.producto.precio

    class Meta:
        model = CarritoItem
        fields = ('id', 'name', 'quantity', 'price', 'unit', 'total')