__author__ = 'wilson'

from unittest import TestCase
from selenium import webdriver
from selenium.webdriver.common.by import By

class FunctionalTest (TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()

    def test_title(self):
        self.browser.get('http://localhost:8000')
        self.assertIn('Casa Organica', self.browser.title)

    def test_registro(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_registro')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Wilson Ricardo')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Guarin Nava')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('wr.guarin234@uniandes.edu.co')

        direccion = self.browser.find_element_by_id('id_direccion')
        direccion.send_keys('Cra 48 N 22 - 83 casa 5')

        self.browser.find_element_by_xpath("//select[@id='id_ciudad']/option[text()='Bogota']").click()

        usuario = self.browser.find_element_by_id('id_usuario')
        usuario.send_keys('wr.guarin234')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        clave = self.browser.find_element_by_id('id_password_confirm')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        self.browser.implicitly_wait(5)
        span = self.browser.find_element_by_id("id_mensaje")

        self.assertIn('Usuario Registrado', span.text)



    def test_registro_email(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_registro')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Wilson Ricardo')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Guarin Nava')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('estonoesunmail')

        direccion = self.browser.find_element_by_id('id_direccion')
        direccion.send_keys('Cra 48 N 22 - 83 casa 5')

        self.browser.find_element_by_xpath("//select[@id='id_ciudad']/option[text()='Bogota']").click()

        usuario = self.browser.find_element_by_id('id_usuario')
        usuario.send_keys('wr.guarin2341')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        clave = self.browser.find_element_by_id('id_password_confirm')
        clave.send_keys('clave123')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        self.browser.implicitly_wait(5)
        span = self.browser.find_element_by_id("id_mensaje")

        self.assertIn('Ingrese un Email Valido', span.text)

    def test_registro_password(self):
        self.browser.get('http://localhost:8000')
        link = self.browser.find_element_by_id('id_registro')
        link.click()

        nombre = self.browser.find_element_by_id('id_nombres')
        nombre.send_keys('Wilson Ricardo')

        apellidos = self.browser.find_element_by_id('id_apellidos')
        apellidos.send_keys('Guarin Nava')

        correo = self.browser.find_element_by_id('id_correo')
        correo.send_keys('un@email.com')

        direccion = self.browser.find_element_by_id('id_direccion')
        direccion.send_keys('Cra 48 N 22 - 83 casa 5')

        self.browser.find_element_by_xpath("//select[@id='id_ciudad']/option[text()='Bogota']").click()

        usuario = self.browser.find_element_by_id('id_usuario')
        usuario.send_keys('wr.guarin2342')

        clave = self.browser.find_element_by_id('id_password')
        clave.send_keys('clave123')

        clave = self.browser.find_element_by_id('id_password_confirm')
        clave.send_keys('clave1223')

        botonGrabar = self.browser.find_element_by_id('id_grabar')
        botonGrabar.click()

        self.browser.implicitly_wait(5)
        span = self.browser.find_element_by_id("id_mensaje")

        self.assertIn('Las contrasenas deben Coincidir', span.text)